/**
 * This file is part of the PlayEdu.
 * (c) 杭州白书科技有限公司
 */
package xyz.playedu.api.types.mapper;

import lombok.Data;

/**
 * @Author 杭州白书科技有限公司
 *
 * @create 2023/3/15 11:18
 */
@Data
public class CourseCategoryCountMapper {

    private Integer cid;

    private Integer total;
}
