/**
 * This file is part of the PlayEdu.
 * (c) 杭州白书科技有限公司
 */
package xyz.playedu.api.types.paginate;

import lombok.Data;

/**
 * @Author 杭州白书科技有限公司
 *
 * @create 2023/2/23 11:34
 */
@Data
public class AdminUserPaginateFilter {

    private String name;

    private Integer roleId;
}
