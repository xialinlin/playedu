/**
 * This file is part of the PlayEdu.
 * (c) 杭州白书科技有限公司
 */
package xyz.playedu.api.request.backend.types;

public interface ImageCaptchaRequestInterface {

    String getCaptchaValue();

    String getCaptchaKey();
}
