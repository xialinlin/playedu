/**
 * This file is part of the PlayEdu.
 * (c) 杭州白书科技有限公司
 */
package xyz.playedu.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;

import xyz.playedu.api.domain.Course;
import xyz.playedu.api.types.paginate.CoursePaginateFiler;

import java.util.List;

/**
 * @author tengteng
 * @description 针对表【courses】的数据库操作Mapper
 * @createDate 2023-03-20 14:25:31 @Entity xyz.playedu.api.domain.Course
 */
@Mapper
public interface CourseMapper extends BaseMapper<Course> {

    List<Course> paginate(CoursePaginateFiler filer);

    Long paginateCount(CoursePaginateFiler filer);

    List<Course> openCoursesAndShow(Integer limit);
}
