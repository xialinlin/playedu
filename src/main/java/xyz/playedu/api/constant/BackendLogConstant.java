/**
 * This file is part of the PlayEdu.
 * (c) 杭州白书科技有限公司
 */
package xyz.playedu.api.constant;

/**
 * @Author 杭州白书科技有限公司
 *
 * @create 2023/2/17 15:42
 */
public class BackendLogConstant {

    public static final String OPT_LOGIN = "LOGIN";

    public static final String MODULE_LOGIN = "LOGIN";
}
